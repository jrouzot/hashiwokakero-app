# Hashiwokakero app

## Overview 

This mobile application that aims to solve hand written hashiwokakero grid, that the user send directly with his smartphone. 
The solving algorithm is run on a distant server, hosted in INSA Toulouse private cloud. This project is a semester project for the students of 
5SDBD at INSA Toulouse.

## Installation

Install expo (https://expo.dev/).
Install nodejs (https://nodejs.org/en/).
Clone the project and run npm i in a terminal at the root of the project to instal the dependencies.
Run expo start to build the app. (See expo guide for more informations)

## Implementation

There is two main view in this application. The picture component (picture-components/Picture.js) allows the user to use his camera to take a picture of the grid and send it to our server. The grid component (grid-components/Grid.js) will handle to draw the grid representation received.

## License

Fell free to use and modify this application :) 
MIT License. (https://github.com/expo/expo/blob/master/LICENSE)