IP = "195.83.10.29"
PORT = "80"

export async function upload(image) {

  /*
  const data = new FormData();
  data.append("image", image);

  const url = IP + ":" + PORT + "/" + ROUTE;

  return await fetch(url, {
    method: "POST",
    body: data,
  });
  */

  console.log("Sending the picture ...")

  try {

    const options = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({image: image.base64})
    };

    const response = await fetch('http://' + IP + ':' + PORT + '/', options)

    let json = await response.json()
    console.log(json)

    let obj = await JSON.parse(json)
    console.log(obj)

    return obj

  } catch (error) {

    console.log(error)
    return error

  }

}