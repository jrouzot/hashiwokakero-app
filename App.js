import React, { useRef, useState } from 'react'
import { Dimensions, Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import * as Progress from 'react-native-progress';
import Grid from './grid-components/Grid'

import { addGridCenters, addGridIslandsSizes } from './grid-utils/utils'
import Picture from './picture-components/Picture'

export default function App() {

  // Calculate the aspect ratio
  const dimensions = useRef(Dimensions.get("window"))
  const width = dimensions.current.width

  const [grid, setGrid] = useState(null)
  const [loading, setLoading] = useState(false)
  const [serverStatus, setServerStatus] = useState("ok")

  const onResponse = (res) => {

    if(res.dimensions) {

      // Get the centers coordinates
      res = addGridIslandsSizes(res, width)
      res = addGridCenters(res, width)
      setGrid(res)

    } else {

      setServerStatus(res.message)
    
    }

    setLoading(false)

  }

  if (serverStatus !== "ok") {

    return (

      <View style={styles.container}>

        <Text style={styles.textError}>{serverStatus}</Text>

        <View style={styles.buttonContainer}>

          {/* Retry button */}
          <TouchableOpacity style={styles.button} onPress={() => {setServerStatus("ok")}}>
            <Text style={styles.text}>Retry</Text>
          </TouchableOpacity>

        </View>

      </View>

    );

  }


  if(grid) {

    return (

      <Grid grid={grid} onRetry={setGrid} />
  
    )
  
  } else if(loading) {

    return(

      <View style={styles.containerLoading}>
        <Progress.Circle size={100} indeterminate={true} borderWidth={6}/>
      </View>

    )

  } else {

    return(

      <Picture onResponse={onResponse} setLoading={setLoading} setServerStatus={setServerStatus} />

    )

  }

}

const styles = StyleSheet.create({

  containerLoading: {

    flex: 1,
    justifyContent: "center",
    alignItems: "center"

  },

  container: {
    
    flex: 1,
    marginTop: 50,
    padding: 10,
    width: "100%",
    height: "100%"
  
  },

  loadingContainer: {

    flex: 1,
    justifyContent: "center",
    alignItems: "center"

  },

  buttonContainer: {

      position: 'absolute',
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'flex-end',
      margin: 80,
      bottom: 0
  
    },
  
    button: {
  
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: 12,
      paddingHorizontal: 64,
      borderRadius: 4,
      elevation: 3,
      backgroundColor: 'blue',
      marginTop: 40
  
    },
  
    text: {
  
      fontSize: 16,
      lineHeight: 21,
      fontWeight: 'bold',
      letterSpacing: 0.25,
      color: 'white',
  
    },

    textError: {
      flex: 1,
      alignSelf: 'center',
      textAlignVertical: 'center',
      fontSize: 22,
      lineHeight: 21,
      fontWeight: 'bold',
      letterSpacing: 0.25,
      color: 'red',
  
    }

});
