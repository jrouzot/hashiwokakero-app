import React from 'react'
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native'

import { Island } from "../grid-components/Island"
import { Bridge } from "../grid-components/Bridge"

import uuid from 'react-native-uuid'
import Svg from 'react-native-svg'

export default function Grid(props) {

    const islandSize = props.grid.islandsSize

    return (

        <View style={styles.container}>
            <Svg style={styles.container}>
                {
                    props.grid.islands.map((island) => {

                        if (island.population !== 0) {

                            return (

                                <Island
                                    key={island.position}
                                    size={islandSize}
                                    label={island.population}
                                    x={island.x - (islandSize / 2)}
                                    y={island.y - (islandSize / 2)}
                                />

                            )

                        }

                    })
                }
                {
                    props.grid.islands.map((island) => {

                        return (

                            island.connections.map((connection) => {

                                return (

                                    <Bridge
                                        key={uuid.v4()}
                                        x1={island.x}
                                        y1={island.y}
                                        x2={props.grid.islands[connection.to].x}
                                        y2={props.grid.islands[connection.to].y}
                                        type={connection.type}
                                    />

                                )

                            })

                        )

                    })

                }
            </Svg>

            <View style={styles.buttonContainer}>

                {/* Retry button */}
                <TouchableOpacity style={styles.button} onPress={() => props.onRetry(null)}>
                    <Text style={styles.text}>Retry</Text>
                </TouchableOpacity>

            </View>

        </View>

    )

}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        marginTop: 50
    },

    buttonContainer: {

        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'flex-end',
        margin: 80,
        bottom: 0
    
      },
    
      button: {
    
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 64,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'blue',
        marginTop: 40
    
      },
    
      text: {
    
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
    
      }

});
