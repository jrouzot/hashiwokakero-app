export function addGridCenters(grid, containerWidth) {

    let x = []
    let y = []

    console.log(grid)
    console.log(grid.dimensions)

    for(let i = 1; i <= grid.dimensions; i++) {

        // Compute x position for each center
        y = containerWidth / (grid.dimensions + 1) * (i)

        for(let j = 1; j <= grid.dimensions; j++) {

            // Compute y position for each center
            x = containerWidth / (grid.dimensions + 1) * (j)

            // Add x and y label for the island centers
            grid.islands[(i - 1) * grid.dimensions + (j - 1)].x = x;
            grid.islands[(i - 1) * grid.dimensions + (j - 1)].y = y;

        }

    }

    return grid

}

export function addGridIslandsSizes(grid, containerWidth) {

    // Compute the island diameter from the dimensions and the number of columns
    let islandSize = containerWidth / (grid.dimensions * 1.8)
    grid.islandsSize = islandSize
    console.log(islandSize)

    return grid

} 